Source: libxt
Section: x11
Priority: optional
Maintainer: Debian X Strike Force <debian-x@lists.debian.org>
Build-Depends: debhelper (>= 9),
               dh-autoreconf,
               libglib2.0-dev (>= 2.16),
               libice-dev (>= 1:0.99.0),
               libsm-dev (>= 1:0.99.1),
               libx11-dev (>= 1:0.99.2),
               pkg-config,
               quilt,
               xmlto (>= 0.0.20),
               xorg-sgml-doctools (>= 1:1.10),
               xutils-dev (>= 1:7.6+3)
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/git/pkg-xorg/lib/libxt
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-xorg/lib/libxt.git

Package: libxt6
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: X11 toolkit intrinsics library
 libXt provides the X Toolkit Intrinsics, an abstract widget library upon
 which other toolkits are based.  Xt is the basis for many toolkits, including
 the Athena widgets (Xaw), and LessTif (a Motif implementation).
 .
 More information about X.Org can be found at:
 <URL:http://www.X.org>
 .
 This module can be found at
 git://anongit.freedesktop.org/git/xorg/lib/libXt

Package: libxt6-dbg
Section: debug
Architecture: any
Priority: extra
Depends: libxt6 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: X11 toolkit intrinsics library (debug package)
 libXt provides the X Toolkit Intrinsics, an abstract widget library upon
 which other toolkits are based.  Xt is the basis for many toolkits, including
 the Athena widgets (Xaw), and LessTif (a Motif implementation).
 .
 This package contains the debug versions of the library found in libxt6.
 Non-developers likely have little use for this package.
 .
 More information about X.Org can be found at:
 <URL:http://www.X.org>
 .
 This module can be found at
 git://anongit.freedesktop.org/git/xorg/lib/libXt

Package: libxt-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libsm-dev,
         libx11-dev,
         libxt6 (= ${binary:Version}),
         x11proto-core-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: libxt-doc
Description: X11 toolkit intrinsics library (development headers)
 libXt provides the X Toolkit Intrinsics, an abstract widget library upon
 which other toolkits are based.  Xt is the basis for many toolkits, including
 the Athena widgets (Xaw), and LessTif (a Motif implementation).
 .
 This package contains the development headers for the library found in
 libxt6.  Non-developers likely have little use for this package.
 .
 More information about X.Org can be found at:
 <URL:http://www.X.org>
 .
 This module can be found at
 git://anongit.freedesktop.org/git/xorg/lib/libXt

Package: libxt-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}
Description: X11 toolkit intrinsics library (documentation)
 libXt provides the X Toolkit Intrinsics, an abstract widget library upon
 which other toolkits are based.  Xt is the basis for many toolkits, including
 the Athena widgets (Xaw), and LessTif (a Motif implementation).
 .
 This package contains the X Toolkit Intrinsics C API documentation.
